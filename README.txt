Change Login Url
----------------

Description
-----------

This module provides the Administrator to change the default login url of 
drupal to a url of his own choice.

Permissions
-----------

A User with "administer site configuration" permission, can change the login 
url of the module through the configuration settings.

Installation
------------

The "CHANGE LOGIN URL" module alters the default login url, hence depends on 
the User module. To install, place the entire change login url folder into 
your site's modules directory.

From the Drupal administration section, go to Administer -> Modules and enable 
the Change Login Url module.

Configurations
--------------

To change the default login url do the following.

1. Go to Administer -> Configuration -> Configure Login Url.
2. A form with textbox will appear.
3. Input a absolute url of your choice.
4. Click on Submit button and the login url will be changed.

Known incompatibilities
-----------------------

None.

Issues
------

If you have any concerns or found any issue with this module please don't 
hesitate to add them in issue queue.

Maintainers
-----------
The Change Login Url was originally developed by:
Sameer G. Jambhulkar

Current maintainers:
Sameer G. Jambhulkar
